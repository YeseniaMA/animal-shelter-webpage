import React, { Component } from 'react'
import { Collapse, Row, Col, Card, CardBody, ListGroup, ListGroupItem, Media, Carousel,
  CarouselItem,
  CarouselControl,
  CarouselIndicators,
  CarouselCaption } from 'reactstrap';
import './about.css';


export default class About extends Component {
  constructor(props) {
   super(props);
   this.toggle = this.toggle.bind(this);
   this.state = { collapse: false };

  }

  toggle() {
     this.setState(state => ({ collapse: !state.collapse }));
  }


  render(){

    return (

      <div  style={{backgroundColor:'#fffbf0',   backgroundImage: 'url("/assets/gplay.png")', paddingTop: '100px', marginTop:'-100px', paddingBottom:'100px'}}>
      <img style={{width:'100%', height:'500px',
        marginLeft: 'auto',
        marginTop: '-80px',
        marginRight: 'auto'}} src={'/assets/active-animal.jpg'}  alt="something"/>


        <Row>
        <Col sm="8" >
        <Card style={{backgroundColor: 'white', paddingLeft:'60px', paddingRight:'60px', paddingBottom:'35px', paddingTop:'40px', marginLeft:'15px'}}>

          <h3 style={{color:'#DD1C1A'}}><em>Nuestra Visión y Misión</em></h3>
          <hr />
          <p style={{color:'#5f5f5f'}}>
            Villa Michelle, es un albergue sin fines de lucro y recibe animales que necesitan de sus servicios. Nuestros animales serán retenidos por un tiempo razonable, dándole a los mismos un máximo de probabilidad de ser adoptados. Desafortunadamente, hay más perros y gatos que hogares para los mismos. Como último recurso se le aplicará la eutanasia en forma humanitaria y siguiendo las especificaciones del Humane Society de los Estados Unidos (HSUS).
          </p>
          <br />
          <p style={{color:'#5f5f5f'}}>
            El Albergue no puede ni debe servir de hogar permanente para muchos animales. Los animales de compañía, especialmente el perro, además de agua y alimento, necesitan amor y la compañía de su amo. Esto no puede suplirse adecuadamente en un albergue con muchos animales.
          </p>

        </Card>
        <ListGroup style={{marginLeft:'15px', marginTop:'10px'}}>
         <ListGroupItem><h3 style={{color:'#DD1C1A'}}><em>Preguntas Frecuentes</em></h3></ListGroupItem>
         <ListGroupItem tag="button" action onClick={this.toggle}>Cras justo odio</ListGroupItem>
         <Collapse isOpen={this.state.collapse}>
           <Card>
             <CardBody>
             Anim pariatur cliche reprehenderit,
              enim eiusmod high life accusamus terry richardson ad squid. Nihil
              anim keffiyeh helvetica, craft beer labore wes anderson cred
              nesciunt sapiente ea proident.
             </CardBody>
           </Card>
         </Collapse>
         <ListGroupItem tag="button" action>Dapibus ac facilisis in</ListGroupItem>
         <ListGroupItem tag="button" action>Morbi leo risus</ListGroupItem>
         <ListGroupItem tag="button" action>Porta ac consectetur ac</ListGroupItem>
         <ListGroupItem disabled tag="button" action>Vestibulum at eros</ListGroupItem>
       </ListGroup>


        </Col>
        <Col>


          <div style={{marginRight:'20px'}}>
          <Card style={{marginBottom: '15px', padding:'10px'}}>
          <Media>
          <Media left href="#">
            <img style={{ width:'50px', height: '50px', margin:'19px'}} src="/assets/mail.svg"  alt="something"/>
          </Media>
          <Media body>
            <Media style={{paddingTop:'25px', marginBottom:'-13px'}} heading>
              Contáctanos
            </Media>
            <hr />
            <ul>
                <li>787-777-7777</li>
                <li>email@villamichelle.com</li>
                <li>Horario: 8:00am - 5:00pm</li>
            </ul>
          </Media>
          </Media>
          </Card>

           </div>
        </Col>


       </Row>
       <Row>

      </Row>
      </div>

    )
  }
}
