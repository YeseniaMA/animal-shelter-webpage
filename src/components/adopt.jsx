import React, { Component } from 'react'
import { Card, CardImg, CardText, CardBody,
  CardTitle, CardSubtitle, Button, Row, Modal, ModalBody, ModalHeader, ModalFooter } from 'reactstrap';
import PetForm from './PetForm';


export default class Adopt extends Component {


  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      pets: []
    };

    this.toggle = this.toggle.bind(this);
  }


  componentDidMount(){
    this.getPets();
  }

  getPets = _ => {
      fetch('http://localhost:3002/pets')
      .then(response => response.json())
      .then(response => this.setState({ pets: response }))
      .catch(err => console.error(err))

  }

  addPet = _ => {

    fetch("http://localhost:3333/pets", {
       method: 'POST',
       headers: {
         'Accept': 'application/json',
         'Content-Type': 'application/json',
       },
       body: JSON.stringify(this.state.pet)
     })
    .catch(err => console.error(err))
  }


  deletePet = id => {
    fetch('http://localhost:3333/pets/' + id, {
    method: 'DELETE'
  })}

  updatePet = id => {
    fetch('http://localhost:3333/pets/' + id, {
    method: 'PUT'
  })}


  renderPet = ({id, name, age}) => <div style={{marginLeft:'15px', marginRight:'5px'}}key={id}>

      <Card style={{width: '300px', margin:'5px'}}>
        <img top style={{width:"299px", height:"200px"}} src="/assets/61369123_1334453240041386_6834096758180544512_o.jpg" alt="{name}" />
        <CardBody>
          <h4>{name}</h4>
          <h4>ID: {id}</h4>
          <CardSubtitle>Edad: </CardSubtitle>
          <CardSubtitle>Nivel de Energia: </CardSubtitle>
          <br />
          <Button>Ver mas</Button>
          <Button style={{marginLeft:'5px'}} onClick={() => { this.deletePet(id) }}>Borrar</Button>
          <Button style={{marginLeft:'5px'}} onClick={() => {  }}>Modificar</Button>
        </CardBody>
      </Card>
    </div>

    toggle() {
   this.setState(prevState => ({
     modal: !prevState.modal
   }));
 }


  render(){
    const { pets } = this.state;
    return (

    <div style={{margin:'15px'}}>
    <Row style={{margin:'15px'}}>
      <h3> Pets </h3>
      <Button style={{marginLeft:'40px'}} onClick={this.toggle}>Añadir</Button>
      <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className} >
          <ModalHeader toggle={this.toggle}>Añadir Mascota</ModalHeader>
          <PetForm />
          <ModalFooter>
            <Button color="primary" onClick={this.addPet}>Añadir</Button>{' '}
            <Button color="secondary" onClick={this.toggle}>Cerrar</Button>
          </ModalFooter>
        </Modal>
    </Row>
      <Row>
      {pets.map(this.renderPet)}
      {console.log(pets)}
      </Row>


    </div>
    )
  }
}
