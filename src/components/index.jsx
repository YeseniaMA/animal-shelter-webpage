import React, { Component } from 'react'

import { Jumbotron, Row, Col, Button, Card } from 'reactstrap';
import './index.css';


export default class Home extends Component {
  render(){
    return (
      <div style={{backgroundImage: 'url("/assets/adorable-animal-animal-photography-1446219.jpg")'}}>
      <Jumbotron style={{backgroundColor: 'transparent'}}>

      <Row>
        <Col sm="4" style={{marginBottom:'50px'}}>
        <h1 className="display-3">Adopta</h1>
        <p className="lead">Dales la oportunidad que se merecen!</p>
        <hr className="my-2" />
        <p>En Puerto Rico hay aproximadamente 12345 mascotas en la calle. Cuando adoptas, no solo salvas una vida sino que brindas la oportunidad a que nuestro
        albergue pueda acoger otro animalito sin hogar.  </p>
        <p className="lead">
          <Button style={{backgroundColor: '#ffaf00', border: '#ffaf00'}} href="http://localhost:3000/adopt">Adopta hoy</Button>
        </p>
        </Col>


          </Row>
      </Jumbotron>


      <div>
        <Row style={{marginTop: '-70px', backgroundColor:'transparent'}}>
          <Col>
            <Card style={{backgroundColor: 'white'}}>
              <div class="text-center" style={{marginTop: '0px', marginRight: '15px',  marginLeft: '10px', backgroundColor:'#ffffff', padding:'30px'}}>
                <img src="/assets/support.svg" alt="icon name" />
                <h3 style={{color:'#DD1C1A'}}>Voluntarios</h3>
                <hr />
                <div>
                  <p style={{paddingLeft: '10px', paddingRightt: '10px'}}>
                    Hay muchas formas en las que se puede ayudar.
                    Desde bañar y acariciar los perros, hasta simplemente jugar con nuestros animales.
                  </p>
                </div>
                <br />
                <Button style={{backgroundColor: '#0091cf', border: '#0091cf'}} href="http://localhost:3000/voluntarios">Ser voluntario</Button>{' '}
              </div>
            </Card>
          </Col>
          <Col>
            <Card style={{backgroundColor: 'white'}}>
              <div class="text-center" style={{marginTop: '0px', marginRight: '15px',  marginLeft: '10px', backgroundColor:'#ffffff', padding:'30px'}}>
                <img  src="/assets/save-money.svg" alt="icon name" />
                <h3 style={{color:'#DD1C1A'}}>Donaciones</h3>
                <hr />
                <p style={{paddingLeft: '10px', paddingRightt: '10px'}}>
                  Los animalitos en Villa Michelle dependen grandemente de tus donativos.
                  Puedes donar seguramente a traves de PayPal.
                </p>
                <br />
                <br />
                <Button style={{backgroundColor: '#0091cf', border: '#0091cf'}}>Donar Ahora</Button>{' '}
             </div>
            </Card >
          </Col>
          <Col>
            <Card style={{backgroundColor: 'white'}}>
              <div class="text-center" style={{marginTop: '0px', marginRight: '15px',  marginLeft: '10px', backgroundColor:'#ffffff', padding:'30px'}}>
                <img src="/assets/respect.svg" alt="icon name" />
                <h3 style={{color:'#DD1C1A'}}>Membresia</h3>
                <hr />
                <p style={{paddingLeft: '10px', paddingRightt: '10px'}}>
                  Hacerse miembro de APAYPA nos ayuda no solo con nuestras finanzas pero también nos levanta la moral grandemente.
                </p>
                <br />
                <br />
                <Button style={{backgroundColor: '#0091cf', border: '#0091cf'}}>Ser Miembro</Button>{' '}
              </div>
            </Card >
          </Col>
          <Col>
            <Card style={{backgroundColor: 'white'}}>
              <div class="text-center" style={{marginTop: '0px', marginRight: '15px',  marginLeft: '10px', backgroundColor:'#ffffff', padding:'30px'}}>
                <img src="/assets/event.svg" alt="icon name" />
                <h3 style={{color:'#DD1C1A'}}>Eventos</h3>
                <hr />
                <p style={{paddingLeft: '10px', paddingRightt: '10px'}}>
                  Descubre eventos de adopcion cerca de ti asi como otras actividades para las mascotas.  También compartimos los resultados del Sato Show.
                </p>
                <br />
                <Button style={{backgroundColor: '#0091cf', border: '#0091cf'}}>Ser Miembro</Button>{' '}
              </div>
            </Card >
          </Col>
          </Row>
        <div  style={{backgroundColor:'#fffbf0',   backgroundImage: 'url("/assets/gplay.png")', paddingBottom: '70px', marginTop: '-330px'}}>>
          <Row style={{marginTop:'340px'}}>
            <Col sm="5" >
              <Card style={{backgroundImage: 'url("/assets/61852983_1334453186708058_8108521487431892992_o.jpg")', border:'transparent', height:'395px', backgroundSize: 'cover', backgroundPosition: 'center'}} />
            </Col>
            <Col>
              <Card style={{backgroundColor: 'white', paddingLeft:'60px', paddingBottom:'35px', paddingTop:'40px', marginLeft:'-30px', border:'transparent'}}>

                  <h3 style={{color:'#DD1C1A'}}><em>Villa Michelle</em></h3><h4 style={{color:'#5f5f5f'}}> es un albergue de animales operado por la organización sin fines de lucro: Asociación Pro Albergue Protección de Animales (APAYPA)
                </h4>
                <h5 style={{color:'#DD1C1A'}}>
                  <em>Estamos localizados en Mayagüez, Puerto Rico.</em>
                </h5>
                <hr />
                <p style={{color:'#5f5f5f'}}>
                  Nos dedicamos a:
                  <ul>
                    <li>Darle albergue a los animales desamparados.</li>
                    <li>Encontrarles hogar.</li>
                    <li>Clínica de esterilización a bajo costo.</li>
                    <li>Promover el cuido responsable por medio de nuestros programas de educación.</li>
                  </ul>
                </p>
              </Card>
            </Col>
         </Row>
       </div>
      </div>
    </div>
    )
  }
}
