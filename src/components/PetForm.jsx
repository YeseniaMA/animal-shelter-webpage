import React, { Component } from 'react';
import { Collapse, Button, CardBody, Card, Progress, Col, Row } from 'reactstrap';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';

import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';

import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';

import Checkbox from '@material-ui/core/Checkbox';

import { Icon } from 'semantic-ui-react'



const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  textField: {
   marginLeft: theme.spacing(1),
   marginRight: theme.spacing(1),
 }
}));




export default class PetForm extends Component {
 state ={
   pet: {
     pet_name: '',
     pet_birthdate: '',
     pet_sex:'',
     pet_type:'',
     pet_spayed: false,
     pet_special_needs: false,
     pet_status: '',
     pet_energy_level: '',
     pet_description: ''
   },
   collapse1: true,
   collapse2: false,
   collapse3: false,




 }
 constructor(props) {
   super(props);
   this.toggle1 = this.toggle1.bind(this);
   this.toggle2 = this.toggle2.bind(this);
   this.toggle3 = this.toggle3.bind(this);

 }

 toggle1() {
   this.setState(state => ({ collapse1: !state.collapse1 }));
 }
 toggle2() {
   this.setState(state => ({ collapse2: !state.collapse2 }));
 }
 toggle3() {
   this.setState(state => ({ collapse3: !state.collapse3 }));
 }





 addPet = _ => {

   fetch("http://localhost:3333/pets", {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(this.state.pet)
    })
   .catch(err => console.error(err))
 }


render (){
  const { pet } = this.state;


return (


    <div  style={{justifyContent: 'center', padding:'30px', height:'900px'}}>
        <Row style={{height:'300px'}}>
            <Card>
            <Row>
              <div style={{width:'5px', height:'80px', backgroundColor:'#5f5f5f', marginLeft:'14px', borderTopLeftRadius: '2px', borderBottomLeftRadius: '2px'}}/>
              <h4 style={{marginTop:'15px', marginLeft:'7px'}}>Información Básica </h4>
              <img src="/assets/chevron-sign-down.svg" style={{width:'20px', height:'20px', marginLeft:'240px', marginTop:'19px'}} onClick={this.toggle1} alt="icon name" />
            </Row>
              <Collapse isOpen={this.state.collapse1}>
                <div style={{justifyContent: 'center', marginTop:'-82px'}}>
                  <TextField
                  id="outlined-name"
                  label="Nombre"
                  value={pet.pet_name}
                  onChange={e => this.setState({ pet: {...pet, pet_name: e.target.value}})}
                  margin="normal"
                  />
                </div>
                <div style={{display: 'flex', justifyContent: 'center', margin: '10px'}}>
                  <TextField style={{width:'200px'}}
                    id="date"
                    label="Fecha de Nacimiento"
                    type="date"
                    defaultValue="2017-05-24"
                    onChange={e => this.setState({ pet: {...pet, pet_birthdate: e.target.value}})}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                </div>
                <div style={{display: 'flex', justifyContent: 'center', marginTop: '25px'}}>
                  <InputLabel style={{marginRight:'10px', marginLeft:'-15px'}} htmlFor="age-simple">Género:</InputLabel>
                   <Select style={{width:'120px'}}
                     value={pet.pet_sex}
                     onChange={e => this.setState({ pet: {...pet, pet_sex: e.target.value}})}>

                   >
                     <MenuItem value="female">Femenino</MenuItem>
                     <MenuItem value="male">Masculino</MenuItem>
                   </Select>
                </div>
                <div style={{display: 'flex', justifyContent: 'center', marginTop: '25px', marginBottom:'40px'}}>
                  <InputLabel style={{marginRight:'10px', marginLeft:'-15px'}} htmlFor="age-simple">Tipo:</InputLabel>
                   <Select style={{width:'120px'}}
                     value={pet.pet_type}
                     onChange={e => this.setState({ pet: {...pet, pet_type: e.target.value}})}>
                   >
                     <MenuItem value="perro">Perro</MenuItem>
                     <MenuItem value="gato">Gato</MenuItem>
                     <MenuItem value="otro">Otro</MenuItem>
                   </Select>
                </div>

              </Collapse>
            </Card>
          </Row>

          <div style={{display: 'flex', justifyContent: 'center'}}>
          <Row>
              <Card>
              <Row>
                <div style={{width:'5px', height:'80px', backgroundColor:'#5f5f5f', marginLeft:'14px', borderTopLeftRadius: '2px', borderBottomLeftRadius: '2px'}}/>
                <h4 style={{marginTop:'15px', marginLeft:'7px'}}>Información de Salud</h4>
                 <img src="/assets/chevron-sign-down.svg" style={{width:'20px', height:'20px', marginLeft:'312px', marginTop:'19px'}} onClick={this.toggle2} alt="icon name" />
              </Row>
                <Collapse isOpen={this.state.collapse2}>

                  <h5 style={{marginLeft:'20px'}}>Marque todas la que apliquen:</h5>
                  <div style={{marginLeft:'35px', marginRight:'30px', marginBottom:'30px'}}>

                  <FormControlLabel
                    value="start"
                    control={<Checkbox
                        onChange={e => this.setState({ pet: {...pet, pet_spayed: e.target.true}})}
                        color="default"
                        value="checkedG"
                        inputProps={{
                          'aria-label': 'checkbox with default color',
                        }}
                      />}
                    label="Esterilizado/a"
                    labelPlacement="end"
                  />


                  <FormControlLabel
                    value="start"
                    control={<Checkbox
                        onChange={e => this.setState({ pet: {...pet, pet_special_needs: e.target.true}})}
                        color="default"
                        value="checkedG"
                        inputProps={{
                          'aria-label': 'checkbox with default color',
                        }}
                      />}
                    label="Necesidades Especiales"
                    labelPlacement="end"
                  />


                  <h6 style={{display: 'flex'}}>Vacunas de perros:</h6>


                      <FormControlLabel
                        value="start"
                        control={<Checkbox

                            color="default"
                            value="checkedG"
                            inputProps={{
                              'aria-label': 'checkbox with default color',
                            }}
                          />}
                        label="Rabia"
                        labelPlacement="end"
                      />

                      <FormControlLabel
                        value="start"
                        control={<Checkbox


                            value="checkedG"
                            inputProps={{
                              'aria-label': 'checkbox with default color',
                            }}
                          />}
                        label="Moquillo"
                        labelPlacement="end"
                      />

                      <FormControlLabel
                        value="start"
                        control={<Checkbox


                            color="default"
                            value="checkedG"
                            inputProps={{
                              'aria-label': 'checkbox with default color',
                            }}
                          />}
                        label="Hepatitis"
                        labelPlacement="end"
                      />

                      <FormControlLabel
                        value="start"
                        control={<Checkbox

                          color="default"
                            value="checkedG"
                            inputProps={{
                              'aria-label': 'checkbox with default color',
                            }}
                          />}
                        label="Leptospirosis"
                        labelPlacement="end"
                      />

                      <FormControlLabel
                        value="start"
                        control={<Checkbox

                            onChange={e => this.setState({ pet: {...pet, spayed_neutered: e.target.true}})}
                            color="default"
                            value="checkedG"
                            inputProps={{
                              'aria-label': 'checkbox with default color',
                            }}
                          />}
                        label="Parainfluenza"
                        labelPlacement="end"
                      />

                      <FormControlLabel
                        value="start"
                        control={<Checkbox

                            onChange={e => this.setState({ pet: {...pet, spayed_neutered: e.target.true}})}
                            color="default"
                            value="checkedG"
                            inputProps={{
                              'aria-label': 'checkbox with default color',
                            }}
                          />}
                        label="Parvovirus"
                        labelPlacement="end"
                      />

                    <h6 style={{display: 'flex'}}>Vacunas de gatos:</h6>
                    <FormControlLabel
                      value="start"
                      control={<Checkbox

                          onChange={e => this.setState({ pet: {...pet, spayed_neutered: e.target.true}})}
                          color="default"
                          value="checkedG"
                          inputProps={{
                            'aria-label': 'checkbox with default color',
                          }}
                        />}
                      label="Rabia"
                      labelPlacement="end"
                    />
                    <FormControlLabel
                      value="start"
                      control={<Checkbox

                          onChange={e => this.setState({ pet: {...pet, spayed_neutered: e.target.true}})}
                          color="default"
                          value="checkedG"
                          inputProps={{
                            'aria-label': 'checkbox with default color',
                          }}
                        />}
                      label="Panleucopenia"
                      labelPlacement="end"
                    />

                    <FormControlLabel
                      value="start"
                      control={<Checkbox

                          onChange={e => this.setState({ pet: {...pet, spayed_neutered: e.target.true}})}
                          color="default"
                          value="checkedG"
                          inputProps={{
                            'aria-label': 'checkbox with default color',
                          }}
                        />}
                      label="Rinotraqueitis"
                      labelPlacement="end"
                    />

                    <FormControlLabel
                      value="start"
                      control={<Checkbox

                          onChange={e => this.setState({ pet: {...pet, spayed_neutered: e.target.true}})}
                          color="default"
                          value="checkedG"
                          inputProps={{
                            'aria-label': 'checkbox with default color',
                          }}
                        />}
                      label="Calicivuis"
                      labelPlacement="end"
                    />

                    <FormControlLabel
                      value="start"
                      control={<Checkbox

                          onChange={e => this.setState({ pet: {...pet, spayed_neutered: e.target.true}})}
                          color="default"
                          value="checkedG"
                          inputProps={{
                            'aria-label': 'checkbox with default color',
                          }}
                        />}
                      label="Clamydia"
                      labelPlacement="end"
                    />
                  </div>
                </Collapse>
              </Card>
            </Row>
            </div>
            <div style={{display: 'flex', justifyContent: 'center'}}>
            <Row>
                <Card >
                <Row>
                  <div style={{width:'5px', height:'80px', backgroundColor:'#5f5f5f', marginLeft:'14px', borderTopLeftRadius: '2px', borderBottomLeftRadius: '2px'}}/>
                  <h4 style={{marginTop:'15px', marginLeft:'7px'}}>Detalles de Mascota</h4>
                  <img src="/assets/chevron-sign-down.svg" style={{width:'20px', height:'20px', marginLeft:'325px', marginTop:'19px'}} onClick={this.toggle3} alt="icon name" />

                </Row>
                  <Collapse isOpen={this.state.collapse3}>
                  <div style={{display: 'flex', justifyContent: 'center', marginTop: '20px'}}>
                    <InputLabel style={{marginRight:'10px', marginLeft:'-15px'}} htmlFor="age-simple">Estatus de adopcion:</InputLabel>
                     <Select style={{width:'120px'}}
                       value={pet.pet_status}
                       onChange={e => this.setState({ pet: {...pet, pet_status: e.target.value}})}>

                     >
                       <MenuItem value="for adoption">Para adopcion</MenuItem>
                       <MenuItem value="foster">Foster</MenuItem>
                       <MenuItem value="deceased">Defuncion</MenuItem>
                       <MenuItem value="hospitalized">Hospitalizado</MenuItem>
                       <MenuItem value="other">Otro</MenuItem>
                     </Select>
                  </div>
                  <div style={{display: 'flex', justifyContent: 'center', marginTop: '25px'}}>
                    <InputLabel style={{marginRight:'10px', marginLeft:'-15px'}} htmlFor="age-simple">Nivel de energia:</InputLabel>
                     <Select style={{width:'120px'}}
                       value={pet.pet_energy_level}
                       onChange={e => this.setState({ pet: {...pet, pet_energy_level: e.target.value}})}>

                     >
                       <MenuItem value="low">Bajo(1 hora ejercicio)</MenuItem>
                       <MenuItem value="medium">Mediano(1-2 horas)</MenuItem>
                       <MenuItem value="high">Alto (2+ horas)</MenuItem>

                     </Select>
                  </div>

                  <div style={{display: 'flex', justifyContent: 'center', marginTop: '20px'}}>
                    <InputLabel style={{marginRight:'5px', display: 'flex', marginTop: '15px'}} >Descripcion:</InputLabel>
                    <TextField style={{marginBottom:'40px', marginTop:'5px'}}
                     id="outlined-multiline-static"
                     multiline
                     rows="4"
                     margin="normal"
                     variant="outlined"
                     value={pet.pet_description}
                     onChange={e => this.setState({ pet: {...pet, pet_description: e.target.value}})}
                   />
                  </div>




                  </Collapse>
                </Card>
              </Row>
              </div>


          <Button  style={{display: 'flex', justifyContent: 'center', margin:'30px'}} color="secondary">Siguiente >> </Button>






    </div>

  )
}
}
