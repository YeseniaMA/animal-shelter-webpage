import React, { Component } from 'react'
import { Collapse, Row, Col, Card, CardBody, ListGroup, ListGroupItem, Media, Carousel,
  CarouselItem,
  CarouselControl,
  CarouselIndicators,
  CarouselCaption } from 'reactstrap';
import './about.css';

const items = [
  {
    id: 1,
    src: '/assets/53499966_1277322125754498_5264433984137330688_n.jpg',
    altText: 'Slide 1',
    caption: 'Slide 1'
  },
  {
    id: 2,
    src: '/assets/57456894_1306371536182890_4689653929846243328_o.jpg',
    altText: 'Slide 2',
    caption: 'Slide 2'
  },
  {
    id: 3,
    src: '/assets/58654713_1310406155779428_5969663040000360448_o.jpg',
    altText: 'Slide 3',
    caption: 'Slide 3'
  }
];

export default class Voluntarios extends Component {
  constructor(props) {
   super(props);
   this.toggle = this.toggle.bind(this);
   this.state = { collapse: false };
   this.state = { activeIndex: 0 };
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
    this.goToIndex = this.goToIndex.bind(this);
    this.onExiting = this.onExiting.bind(this);
    this.onExited = this.onExited.bind(this);
  }

  toggle() {
     this.setState(state => ({ collapse: !state.collapse }));
  }

  onExiting() {
   this.animating = true;
 }

 onExited() {
   this.animating = false;
 }

 next() {
   if (this.animating) return;
   const nextIndex = this.state.activeIndex === items.length - 1 ? 0 : this.state.activeIndex + 1;
   this.setState({ activeIndex: nextIndex });
 }

 previous() {
   if (this.animating) return;
   const nextIndex = this.state.activeIndex === 0 ? items.length - 1 : this.state.activeIndex - 1;
   this.setState({ activeIndex: nextIndex });
 }

 goToIndex(newIndex) {
   if (this.animating) return;
   this.setState({ activeIndex: newIndex });
 }


  render(){
    const { activeIndex } = this.state;

   const slides = items.map((item) => {
     return (
       <CarouselItem style={{width:'970px', height:'880px'}}
         className="custom-tag"
         tag="div"
         key={item.id}
         onExiting={this.onExiting}
         onExited={this.onExited}
       >
        <img style={{width:'970px', height:'740px', marginTop:'-10px'}} src={item.src} alt={item.altText} />
         <CarouselCaption className="text-danger" captionText={item.caption} captionHeader={item.caption} />
       </CarouselItem>
     );
   });
    return (

      <div  style={{backgroundColor:'#fffbf0',   backgroundImage: 'url("/assets/gplay.png")', paddingTop: '100px', marginTop:'-100px', paddingBottom:'100px'}}>
        <Row>
        <Col>
        <img style={{width:'300px', height:'250px', borderRadius: '50%', display: 'block',
          marginLeft: 'auto',
          marginRight: 'auto'}} src={'/assets/57486579_1305030652983645_8740206615694671872_o.jpg'}  alt="something"/>

          <div style={{marginLeft:'20px'}}>
          <Card style={{marginBottom: '15px', padding:'10px'}}>
          <Media>
          <Media left href="#">
            <img style={{ width:'50px', height: '50px', margin:'19px'}} src="/assets/mail.svg"  alt="something"/>
          </Media>
          <Media body>
            <Media style={{paddingTop:'25px', marginBottom:'-13px'}} heading>
              Contactanos
            </Media>
            <hr />
            <ul>
                <li>787-777-7777</li>
                <li>email@villamichelle.com</li>
                <li>Horario: 8:00am - 5:00pm</li>
            </ul>
          </Media>
          </Media>
          </Card>
             <ListGroup>
              <ListGroupItem><h4 style={{color:'#5f5f5f'}}>Preguntas Frecuentes</h4></ListGroupItem>
              <ListGroupItem tag="button" action onClick={this.toggle}>Cras justo odio</ListGroupItem>
              <Collapse isOpen={this.state.collapse}>
                <Card>
                  <CardBody>
                  Anim pariatur cliche reprehenderit,
                   enim eiusmod high life accusamus terry richardson ad squid. Nihil
                   anim keffiyeh helvetica, craft beer labore wes anderson cred
                   nesciunt sapiente ea proident.
                  </CardBody>
                </Card>
              </Collapse>
              <ListGroupItem tag="button" action>Dapibus ac facilisis in</ListGroupItem>
              <ListGroupItem tag="button" action>Morbi leo risus</ListGroupItem>
              <ListGroupItem tag="button" action>Porta ac consectetur ac</ListGroupItem>
              <ListGroupItem disabled tag="button" action>Vestibulum at eros</ListGroupItem>
            </ListGroup>
           </div>
        </Col>
          <Col sm="8" >
          <Card style={{backgroundColor: 'white', paddingLeft:'60px', paddingRight:'60px', paddingBottom:'35px', paddingTop:'40px', marginRight:'15px'}}>

            <h3 style={{color:'#DD1C1A'}}><em>Nuestra Vision y Mision</em></h3>
            <hr />
            <p style={{color:'#5f5f5f'}}>
              Villa Michelle, es un albergue sin fines de lucro y recibe animales que necesitan de sus servicios. Nuestros animales serán retenidos por un tiempo razonable, dándole a los mismos un máximo de probabilidad de ser adoptados. Desafortunadamente, hay más perros y gatos que hogares para los mismos. Como último recurso se le aplicará la eutanasia en forma humanitaria y siguiendo las especificaciones del Humane Society de los Estados Unidos (HSUS).
            </p>
            <br />
            <p style={{color:'#5f5f5f'}}>
              El Albergue no puede ni debe servir de hogar permanente para muchos animales. Los animales de compañía, especialmente el perro, además de agua y alimento, necesitan amor y la compañía de su amo. Esto no puede suplirse adecuadamente en un albergue con muchos animales.
            </p>

          </Card>
          <div style={{marginRight:'15px', marginTop:'10px'}}>
             <style>
               {
                 `.custom-tag {
                     max-width: 100%;
                     height: 500px;
                     background: black;
                   }`
               }
             </style>
             <Carousel
               activeIndex={activeIndex}
               next={this.next}
               previous={this.previous}
             >
               <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={this.goToIndex} />
               {slides}
               <CarouselControl direction="prev" directionText="Previous" onClickHandler={this.previous} />
               <CarouselControl direction="next" directionText="Next" onClickHandler={this.next} />
             </Carousel>
           </div>

          </Col>

       </Row>
       <Row>

      </Row>
      </div>

    )
  }
}
