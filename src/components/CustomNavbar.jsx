import React, { Component } from 'react';
import { Form, Button, FormControl } from 'react-bootstrap';
import './CustomNavbar.css';

export default class CustomNavbar extends Component {
  render(){
    return (
      <nav style={{backgroundColor: '#ffffff'}} class="navbar navbar-expand-lg sticky-top mb-5">
    <div class="container">
      <a class="navbar-brand" href="http://localhost:3000"><img style={{margin: '0px', marginRight:'15px', width:'40px'}} src="/assets/pet.svg" alt="icon name" /> Villa Michelle</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="http://localhost:3000">Home
                  <span class="sr-only">(current)</span>
                </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="http://localhost:3000/About">About</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="http://localhost:3000">Services</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="http://localhost:3000/About">Contact</a>
          </li>
        </ul>
        <Form inline>
          <FormControl type="text" placeholder="Search" className="mr-sm-2" />
          <Button variant="outline-success">Search</Button>
        </Form>
      </div>
    </div>


  </nav>




  )
}
}
