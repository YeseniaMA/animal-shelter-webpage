import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Route} from 'react-router-dom';
import Home from './components/index';
import About from './components/about';
import CustomNavbar from './components/CustomNavbar';
import Footer from './components/Footer';
import PetForm from './components/PetForm';
import Adopt from './components/adopt.jsx';
import Voluntarios from './components/voluntarios.jsx';




export default class App extends Component {
 render(){

  return (
    <div>
    <CustomNavbar />

    <Router>
        <Route exact path="/" component={Home} />
        <Route path="/petform" component={PetForm} />
        <Route path="/voluntarios" component={Voluntarios} />
        <Route exact path="/about" component={About} />
        <Route exact path="/adopt" component={Adopt} />
    </Router>
    <Footer />
    </div>
  );
}
}
